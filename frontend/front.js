var express = require('express');
var path = require('path');
var url = require('url');
var fs = require('fs');
var multer  = require('multer');
var bodyParser = require('body-parser');
var app = express();
var upload = multer();
var request = require('request-promise');
var querystring = require('querystring');

app.engine('ejs', require('ejs-locals'));
app.use(express.bodyParser());
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(app.router);
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

var auth;

app.use(function(req, res) {
    res.status(404);
    fs.readFile('public/404.html', function(error, data) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
    });
});

app.get('/user/data/js/script.js', (req, res, next) => {
    fs.readFile('public/js/script.js', function(error, data) {
        res.writeHead(200, {'Content-Type': 'text/css'});
        res.write(data);
        res.end();
    });
  });

app.get('/css/bootstrap.css', (req, res, next) => {
    fs.readFile('public/css/bootstrap.css', function(error, data) {
        res.writeHead(200, {'Content-Type': 'text/css'});
        res.write(data);
        res.end();
    });
});

app.get('/css/signin.css', (req, res, next) => {
    fs.readFile('public/css/signin.css', function(error, data) {
        res.writeHead(200, {'Content-Type': 'text/css'});
        res.write(data);
        res.end();
    });
});

app.get('/css/style.css', (req, res, next) => {
    fs.readFile('public/css/style.css', function(error, data) {
        res.writeHead(200, {'Content-Type': 'text/css'});
        res.write(data);
        res.end();
    });
});

app.get('/image/cmd_logo.png', (req, res, next) => {
    fs.readFile('public/image/cmd_logo.png', function(error, data) {
        res.writeHead(200, {'Content-Type': 'image/png'});
        res.write(data);
        res.end();
    });
});

app.get('/user/public/image/envelope.jpg', (req, res, next) => {
    fs.readFile('public/image/envelope.jpg', function(error, data) {
        res.writeHead(200, {'Content-Type': 'image/jpg'});
        res.write(data);
        res.end();
    });
});

app.get(['/login', '/'], (req, res, next) => {
    fs.readFile('public/login.html', function(error, data) {
        if (error) {
            res.writeHead(404);
            res.write(error);
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            res.end();
        }
    });
});

app.get('/user/data/view', (req, res) => {
   if(auth == undefined) {
      res.redirect("/login");
   } else {
      fs.readFile('public/getdatafilter.html', function(error, data) {
          if (error) {
              res.writeHead(404);
              res.write(error);
              res.end();
          } else {
              res.writeHead(200, {'Content-Type': 'text/html'});
              res.write(data);
              res.end();
          }
      });
  }
});

app.get('/user/personal_data', (req, res) => {
  if(auth == undefined) {
     res.redirect("/login");
  } else {
    const options = {
        method: 'GET',
        uri: 'http://localhost:8880/user/personal_data',
        headers: {
          'Authorization' : 'Basic ' + auth
        }
    }

    request(options, function (error, response, body) {
      var person = JSON.parse(body);
      console.log(person);
        res.render('personaldata', {firstName:person.firstName, lastName:person.lastName ,patronymic:person.patronymic,
          email:person.email, phone:person.phone, street:person.street, house:person.home, room:person.room});
    });
}

});

app.get('/user/data/add', (req, res, next) => {
  if(auth == undefined) {
     res.redirect("/login");
  } else {
      fs.readFile('public/addData.html', function(error, data) {
          if (error) {
              res.writeHead(404);
              res.write(error);
              res.end();
          } else {
              res.writeHead(200, {'Content-Type': 'text/html'});
              res.write(data);
              res.end();
          }
      });
    }
});

app.get('/user/data/get', (req, res, error) => {
    var fromDate = req.url.split('?')[1].split('&')[0].split('=')[1];
    var toDate = req.url.split('?')[1].split('&')[1].split('=')[1];
    var url = 'http://localhost:8880/user/data/get?fromDate=' + fromDate + '&toDate=' + toDate;
    const options = {
        method: 'GET',
        uri: url,
        headers: {
          'Authorization' : 'Basic ' + auth
        }
    }

    request(options, function (error, response, body) {
        if (response.statusCode == 200) {
            var data = JSON.parse(body);
            var people = data.userInfo;
            var baseData = Object.entries(data.metersData);
            var baseDataMap = new Map(baseData);

            if (data.predictionalMetersData != null) {
              var predictionalData = Object.entries(data.predictionalMetersData);
              var predictionalDataMap = new Map(predictionalData);
            }

            var arr = [];
            var d = [];
            var cold = [];
            var hot = [];
            var electricity = [];
            for (var i = 0; i < baseData.length; i++) {
              arr.push(baseData[i][0]);
              a = baseData[i][1];
              hot.push(a.hot_w);
              cold.push(a.cold_w);
              electricity.push(a.electricity);
            }
            res.render('getdata', {myData : baseDataMap, predictionalMetersData: predictionalDataMap, dates : arr, coldW: cold, hotW: hot,
            electr : electricity});
        } else if (response.statusCode == 400) {
          fs.readFile('public/getdatafilter.html', function(error, data) {
              res.writeHead(200, {'Content-Type': 'text/html'});
              var fail = JSON.parse(response.body);
              res.write('<p class="wrond_add">' + fail.message + '</p>');
              res.write(data);
              res.end();
          });
        }
    });
});

app.get('/user/data/view_month', (req, res, error) => {
  if(auth == undefined) {
    res.redirect("/login");
  } else {
    fs.readFile('public/viewmonth.html', function(error, data) {
      if (error) {
        res.writeHead(404);
        res.write(error);
        res.end();
      } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
      }
    });
  }
});

app.get('/user/data/view_month_data', upload.array(), (req, res, error) => {
    const options = {
        method: 'GET',
        uri: 'http://localhost:8880/admin/data?date=' + req.query.date,
        headers: {
            'Authorization' : 'Basic ' + auth
        },
        json: true
    }

    request(options, function (error, response, body) {
      if (response.statusCode == 403) {
        fs.readFile('public/viewmonth.html', function(error, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<p class="no_access">У вас нет доступа к данным!</p>');
            res.write(data);
            res.end();
        });
      }
      if (response.statusCode == 200) {
          var obj = Object.entries(body.roomResponse);
          var map = new Map(obj);
          res.render('viewmonth', {myData : map});
      }

      if (response.statusCode == 400) {
        fs.readFile('public/viewmonth.html', function(error, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<p class="wrond_add">' + response.body.message + '</p>');
            res.write(data);
            res.end();
        });
      }
    });
});

app.post('/user/data/add', upload.array(), (req, res, error) => {
    var form = {
        hotWater: parseInt(req.body.hotWater, 10),
        coldWater: parseInt(req.body.coldWater, 10),
        electricity: parseInt(req.body.electricity, 10),
        date: req.body.date.toString()
    };

    var contentLength = form.length;

    const options = {
        method: 'POST',
        uri: 'http://localhost:8880/user/data/add',
        headers: {
            'content-type': 'application/json; charset=UTF-8',
            'Content-Length': contentLength,
            'Authorization' : 'Basic ' + auth
        },
        body: form,
        json: true
    }

    request(options, function (error, response, body) {
      if (response.statusCode == 401) {
        fs.readFile('public/login.html', function(error, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<p class="wrong_password">Для выполнения этого действия требуется авторизация</p>');
            res.write(data);
            res.end();
        });
      }
      if (response.statusCode == 200) {
          var obj = Object.entries(body.metersData);
          var map = new Map(obj);

          var arr = [];
          var d = [];
          var cold = [];
          var hot = [];
          var electricity = [];
          for (var i = 0; i < obj.length; i++) {
            arr.push(obj[i][0]);
            a = obj[i][1];
            hot.push(a.hot_w);
            cold.push(a.cold_w);
            electricity.push(a.electricity);
          }

          res.render('getdata', {myData : map, predictionalMetersData: undefined,  dates : arr, coldW: cold, hotW: hot,
          electr : electricity});
      } else if (response.statusCode == 400) {
        fs.readFile('public/addData.html', function(error, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<p class="wrond_add">' + response.body.message + '</p>');
            res.write(data);
            res.end();
        });
      }
    });
});

app.post('/user/data/edit', (req, res, error) => {
    var fromDate = req.headers.referer.split("?")[1].split("&")[0].split("=")[1];
    var toDate = req.headers.referer.split("?")[1].split("&")[1].split("=")[1];

    var data = {
      hot: req.body.hot,
      cold: req.body.cold,
      electricity: req.body.electricity,
      date : req.body.date
    }
    console.log("1");

    const options = {
        method: 'PUT',
        uri: 'http://localhost:8880/user/data/edit',
        headers: {
            'content-type': 'application/json; charset=UTF-8',
            'Authorization' : 'Basic ' + auth
        },
        body: data,
        json: true
    }

    console.log("2");

    request(options, function (error, response, body) {
        if (response.statusCode == 200) {
            var url = '/user/data/get?fromDate=' + fromDate + "&toDate=" + toDate;
            res.redirect(url);
        }
    });
});

app.post('/user/data/send', (req, res, error) => {
    var toD = req.headers.referer.split('?')[1].split('&')[1].split('=')[1];
    var fromD = req.headers.referer.split('?')[1].split('&')[0].split('=')[1];

    var url = 'http://localhost:8880/user/data/send?fromDate=' + fromD + '&toDate=' + toD;
    const options = {
        method: 'POST',
        uri: url,
        headers: {
          'Authorization' : 'Basic ' + auth
        }
    }
    request(options, function (error, response, body) {
        res.redirect(req.headers.referer);
    });
});

app.post('/login', upload.array(), (req, res, error) => {
    var form = {
        login: req.body.login,
        password: req.body.password,
    };

    var formData = querystring.stringify(form);
    var contentLength = formData.length;

    const options = {
        method: 'POST',
        uri: 'http://localhost:8880/login',
        headers: {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Content-Length': contentLength,
            'Authorization' : 'Basic ' + auth
        },
        body: formData,
        json: true
    }

    request(options, function (error, response, body) {
        if (response.statusCode == 302) {
            var loc = response.headers.location;
            if (loc == 'http://localhost:8880/'){
                auth = Buffer.from(form.login + ":" + form.password).toString('base64');
                fs.readFile('public/getdatafilter.html', function(error, data) {
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(data);
                    res.end();
                });
            }

            if (loc == 'http://localhost:8880/login?error'){
                fs.readFile('public/login.html', function(error, data) {
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write('<p class="wrong_password">Неправильный логин или пароль</p>');
                    res.write(data);
                    res.end();
                });
            }
        }
    });
});

app.get('/logout', (req, res, error) => {
    const options = {
        method: 'POST',
        uri: 'http://localhost:8880/logout',
        'Authorization' : 'Basic ' + auth
    }

    request(options, function (error, response, body) {
        if (response.statusCode == 302) {
            var loc = response.headers.location;
            if (loc == 'http://localhost:8880/'){
                auth = undefined;
                fs.readFile('public/login.html', function(error, data) {
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(data);
                    res.end();
                });
            }
        }
    });
});

app.listen(8000);
