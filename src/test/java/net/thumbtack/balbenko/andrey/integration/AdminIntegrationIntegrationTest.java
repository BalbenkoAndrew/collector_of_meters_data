package net.thumbtack.balbenko.andrey.integration;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.AdminGetDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.FailureResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.User;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class AdminIntegrationIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testGetMetersDataByMonth() {
        User user = userDao.getUserByLogin("login");
        addData(new Data(user, 5, 5, 5, LocalDate.of(2018, 7, 1)));
        addData(new Data(user, 10, 10, 10, LocalDate.of(2018, 7, 2)));
        addData(new Data(user, 15, 15, 15, LocalDate.of(2018, 7, 3)));
        addData(new Data(user, 20, 20, 20, LocalDate.of(2018, 7, 4)));

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/admin/data")
                .queryParam("date", "2018-07-01");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity<?> entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<AdminGetDataResponseDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, AdminGetDataResponseDto.class);

        Map<Integer, MetersDataResponse> roomResponse = new LinkedHashMap<>();
        roomResponse.put(5, new MetersDataResponse(12, 12, 12));
        AdminGetDataResponseDto responseDto = new AdminGetDataResponseDto(roomResponse, "2018-07-01");

        assertEquals(response.getStatusCode().value(), 200);
        assertEquals(response.getBody(), responseDto);
    }

    @Test
    public void testGetMetersDataByMonthWrongDate() {
        User user = userDao.getUserByLogin("login");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/admin/data")
                .queryParam("date", "ABCDEFG");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<FailureResponse> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, FailureResponse.class);

        assertEquals(response.getBody().getErrorCode(), ErrorCodes.DATE_WRONG.toString());
    }


}
