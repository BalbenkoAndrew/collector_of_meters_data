package net.thumbtack.balbenko.andrey.integration;

import net.thumbtack.balbenko.andrey.controllers.dto.request.EditUserDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.request.UserAddDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.*;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.PredictionalMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.UserInfoResponseDto;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.User;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class UserIntegrationIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testGetData() {
        User user = userDao.getUserByLogin("login");
        addData(new Data(user, 5, 5, 5, LocalDate.of(2018, 7, 1)));
        addData(new Data(user, 10, 10, 10, LocalDate.of(2018, 7, 2)));
        addData(new Data(user, 15, 15, 15, LocalDate.of(2018, 7, 3)));
        addData(new Data(user, 20, 20, 20, LocalDate.of(2018, 7, 4)));


        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/user/data/get")
                .queryParam("fromDate", "2018-07-01")
                .queryParam("toDate", "2018-07-04");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity<?> entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<UserMetersDataResponseDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, UserMetersDataResponseDto.class);

        Map<String, MetersDataResponse> metersData = new LinkedHashMap<>();
        metersData.put("2018-07-01", new MetersDataResponse(5, 5, 5));
        metersData.put("2018-07-02", new MetersDataResponse(10, 10, 10));
        metersData.put("2018-07-03", new MetersDataResponse(15, 15, 15));
        metersData.put("2018-07-04", new MetersDataResponse(20, 20, 20));

        Map<String, PredictionalMetersDataResponseDto> predictionalMetersData = new LinkedHashMap<>();
        predictionalMetersData.put("2018-07-05", new PredictionalMetersDataResponseDto(24, 24, 24));
        predictionalMetersData.put("2018-07-06", new PredictionalMetersDataResponseDto(29, 29, 29));

        UserInfoResponseDto userInfoResponseDto = new UserInfoResponseDto("Иван", "Иванов", null);

        UserMetersDataResponseDto userMetersDataResponseDto = new UserMetersDataResponseDto(metersData, predictionalMetersData, userInfoResponseDto);

        assertEquals(response.getBody().getUserInfo(), userMetersDataResponseDto.getUserInfo());
        assertEquals(response.getBody().getPredictionalMetersData(), userMetersDataResponseDto.getPredictionalMetersData());
        assertEquals(response.getBody().getMetersData(), userMetersDataResponseDto.getMetersData());
    }

    @Test
    public void testGetDataInvalidDate() {
        User user = userDao.getUserByLogin("login");
        addData(new Data(user, 5, 5, 5, LocalDate.of(2018, 7, 1)));
        addData(new Data(user, 10, 10, 10, LocalDate.of(2018, 7, 2)));
        addData(new Data(user, 15, 15, 15, LocalDate.of(2018, 7, 3)));
        addData(new Data(user, 20, 20, 20, LocalDate.of(2018, 7, 4)));


        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/user/data/get")
                .queryParam("fromDate", "ABCD")
                .queryParam("toDate", "EFGH");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity<?> entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<FailureResponse> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, FailureResponse.class);

        assertEquals(response.getBody().getErrorCode(), ErrorCodes.DATE_WRONG.toString());
        assertEquals(response.getBody().getMessage(), ErrorCodes.DATE_WRONG.getMessage());
    }

    @Test
    public void testGetDataFromDateAfterToDate() {
        User user = userDao.getUserByLogin("login");
        addData(new Data(user, 5, 5, 5, LocalDate.of(2018, 7, 1)));
        addData(new Data(user, 10, 10, 10, LocalDate.of(2018, 7, 2)));
        addData(new Data(user, 15, 15, 15, LocalDate.of(2018, 7, 3)));
        addData(new Data(user, 20, 20, 20, LocalDate.of(2018, 7, 4)));


        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/user/data/get")
                .queryParam("fromDate", "2018-07-01")
                .queryParam("toDate", "2018-06-01");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity<?> entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<FailureResponse> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, FailureResponse.class);

        assertEquals(response.getBody().getErrorCode(), ErrorCodes.START_DATE_LONGER_END_DATE.toString());
        assertEquals(response.getBody().getMessage(), ErrorCodes.START_DATE_LONGER_END_DATE.getMessage());
    }

    @Test
    public void testGetDataDataIsEmpty() {
        User user = userDao.getUserByLogin("login");


        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/user/data/get")
                .queryParam("fromDate", "2018-07-01")
                .queryParam("toDate", "2018-07-06");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity<?> entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<FailureResponse> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, FailureResponse.class);

        assertEquals(response.getBody().getErrorCode(), ErrorCodes.DATA_IS_EMPTY.toString());
        assertEquals(response.getBody().getMessage(), ErrorCodes.DATA_IS_EMPTY.getMessage());
    }

    @Test
    public void testGetDataAddData() {
        User user = userDao.getUserByLogin("login");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/user/data/add");

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("10", "15", "20", "2018-07-01");

        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity entity = getHttpEntity(loginAndPassword, userAddDataRequestDto);

        ResponseEntity<AddUserDataResponseDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, AddUserDataResponseDto.class);

        Map<String, MetersDataResponse> metersData = new LinkedHashMap<>();
        metersData.put("2018-07-01", new MetersDataResponse(10, 15, 20));
        UserInfoResponseDto userInfoResponseDto = new UserInfoResponseDto("Иван", "Иванов", null);

        AddUserDataResponseDto addUserDataResponseDto = new AddUserDataResponseDto(metersData, userInfoResponseDto);

        assertEquals(response.getStatusCode().value(), 200);
        assertEquals(response.getBody(), addUserDataResponseDto);
    }

    @Test
    public void testGetPersonalData() {
        User user = userDao.getUserByLogin("login");


        String loginAndPassword = new String(Base64.encodeBase64(loginAndPasswordToString(user).getBytes()));
        HttpEntity entity = getHttpEntity(loginAndPassword, null);

        ResponseEntity<PersonalDataResponseDto> responseEntity = restTemplate.exchange(urlPrefix + "/user/personal_data", HttpMethod.GET, entity, PersonalDataResponseDto.class);
        PersonalDataResponseDto expectedResponse = new PersonalDataResponseDto("Иван", "Иванов", null, "email@gmail.com", "88005553535", "Вавилова", "35", "5");
        assertEquals(responseEntity.getBody(), expectedResponse);
    }
}
