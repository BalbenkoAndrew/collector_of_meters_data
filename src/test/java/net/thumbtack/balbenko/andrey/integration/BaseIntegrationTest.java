package net.thumbtack.balbenko.andrey.integration;

import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.LocationDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.daoimpl.DataDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.LocationDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.UserDaoImpl;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.Location;
import net.thumbtack.balbenko.andrey.domain.User;
import net.thumbtack.balbenko.andrey.exception.GlobalControllerExceptionHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = {"server.port=8000"})
public class BaseIntegrationTest {
    protected static String urlPrefix;
    protected static RestTemplate restTemplate;
    protected DataDao dataDao = new DataDaoImpl();
    protected UserDao userDao = new UserDaoImpl();
    protected LocationDao locationDao = new LocationDaoImpl();

    @BeforeClass
    public static void before() {
        urlPrefix = String.format("http://localhost:%d", 8000);
        restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new GlobalControllerExceptionHandler());
    }

    @Before
    public void login() throws SQLException {
        dataDao.deleteAllData();
        userDao.deleteAllUsers();
        locationDao.deleteAllLocations();

        Location location = new Location("Вавилова", 35, null, new HashSet<>());
        locationDao.addLocation(location);
        userDao.addUser(new User("Иван", "Иванов", null, "ROLE_ADMIN", "login", "pwd", "email@gmail.com", "88005553535", "5", location, new HashSet<>()));
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("login", "login");
        map.add("password", "pwd");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        restTemplate.postForEntity(urlPrefix + "/login", request, String.class);
    }

    public void addData(Data data) {
        dataDao.addUserData(data);
    }

    public String loginAndPasswordToString(User user) {
        return user.getLogin() + ":" + user.getPassword();
    }


    protected static HttpEntity getHttpEntity(String auth, Object request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + auth);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return new HttpEntity<>(request, headers);
    }
}
