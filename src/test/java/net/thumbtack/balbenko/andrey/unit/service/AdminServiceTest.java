package net.thumbtack.balbenko.andrey.unit.service;


import net.thumbtack.balbenko.andrey.controllers.dto.responce.AdminGetDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.LocationDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.daoimpl.DataDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.LocationDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.UserDaoImpl;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.Location;
import net.thumbtack.balbenko.andrey.domain.User;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import net.thumbtack.balbenko.andrey.service.AdminService;
import net.thumbtack.balbenko.andrey.service.BaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AdminServiceTest extends BaseServiceTest {
    private Logger LOGGER = LoggerFactory.getLogger(AdminServiceTest.class);
    private BaseService baseService = new BaseService();

    @InjectMocks
    private UserDao userDao = mock(UserDaoImpl.class);
    @InjectMocks
    private DataDao dataDao = mock(DataDaoImpl.class);
    @InjectMocks
    private LocationDao locationDao = mock(LocationDaoImpl.class);

    private AdminService adminService = new AdminService(locationDao, dataDao, userDao);

    @Test
    public void testGetMetersDataByMonth() throws CmdException {
        String date = "2018-06-01";

        Location location = new Location( "Ленина", 2, null, null);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", location, new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> data = new ArrayList<>();
        data.add(new Data(new User(1, "Петя", "Петров", "Петрович", "ROLE_USER", "pert", "pert", "petr@gmail.com", "88005553535", "5", location, null),10, 10, 10, LocalDate.of(2018,6,15)));

        data.add(new Data(new User(2, "Миша", "Мишкин", "Михайлович", "ROLE_USER", "misha", "misha", "misha@gmail.com", "8800553555", "10", location, null),15, 15, 15, LocalDate.of(2018,6,18)));

        data.add(new Data(new User(3, "Руслан", "Русс", "Русланович", "ROLE_USER", "ruslan", "ruslan", "ruslan@gmail.com", "88009995566", "15", location, null),20, 20, 20, LocalDate.of(2018,6,21)));

        when(dataDao.getDataByMonth(LocalDate.of(2018, 6, 1), LocalDate.of(2018, 6, 30))).thenReturn(data);

        Map<Integer, MetersDataResponse> rooms = new LinkedHashMap<>();

        MetersDataResponse map1 = new MetersDataResponse(10, 10, 10);
        rooms.put(5, map1);
        MetersDataResponse map2 = new MetersDataResponse(15, 15, 15);
        rooms.put(10, map2);
        MetersDataResponse map3 = new MetersDataResponse(20, 20, 20);
        rooms.put(15, map3);

        AdminGetDataResponseDto adminGetDataResponseDto = new AdminGetDataResponseDto(rooms, date);
        AdminGetDataResponseDto result = adminService.getMetersDataByMonth(date);
        assertEquals(adminGetDataResponseDto, result);
    }

    @Test
    public void testWrongDate() throws CmdException {
        String date = "ABCDEFGH";

        Location location = new Location( "Ленина", 2, null, null);
        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", location, new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        try {
            adminService.getMetersDataByMonth(date);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATE_WRONG, ex.getErrorCodes());
        }
    }

    @Test
    public void testEmptyData() throws CmdException {
        String date = "2018-06-01";

        Location location = new Location( "Ленина", 2, null, null);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", location, new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> data = new ArrayList<>();

        when(dataDao.getDataByMonth(LocalDate.of(2018, 6, 1), LocalDate.of(2018, 6, 30))).thenReturn(data);

        try {
            adminService.getMetersDataByMonth(date);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATA_IS_EMPTY, ex.getErrorCodes());
        }
    }


}
