package net.thumbtack.balbenko.andrey.unit.service;

import net.thumbtack.balbenko.andrey.controllers.dto.request.UserAddDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.AddUserDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.UserMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.PredictionalMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.UserInfoResponseDto;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.daoimpl.DataDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.UserDaoImpl;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.Location;
import net.thumbtack.balbenko.andrey.domain.User;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import net.thumbtack.balbenko.andrey.service.BaseService;
import net.thumbtack.balbenko.andrey.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest extends BaseServiceTest {
    private BaseService baseService = new BaseService();

    @InjectMocks
    private UserDao userDao = mock(UserDaoImpl.class);
    @InjectMocks
    private DataDao dataDao = mock(DataDaoImpl.class);

    private UserService userService = new UserService(userDao, dataDao);

    @Test
    public void testGetUserMeterData() throws CmdException {
        LocalDate fromDateL = LocalDate.of(2018, 6, 1);
        LocalDate toDateL = LocalDate.of(2018, 6, 30);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> dataList = new ArrayList<>();
        dataList.add(new Data(user, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        dataList.add(new Data(user, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        when(dataDao.getUserData(fromDateL, toDateL, user.getId())).thenReturn(dataList);

        Map<String, MetersDataResponse> responseData = new HashMap<>();
        responseData.put(dataList.get(0).getDate().toString(), new MetersDataResponse(dataList.get(0).getHot(), dataList.get(0).getCold(), dataList.get(0).getElectricity()));
        responseData.put(dataList.get(1).getDate().toString(), new MetersDataResponse(dataList.get(1).getHot(), dataList.get(1).getCold(), dataList.get(1).getElectricity()));

        UserInfoResponseDto userInfoResponseDto = new UserInfoResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic());

        UserMetersDataResponseDto userMetersDataResponseDto = new UserMetersDataResponseDto(responseData, null, userInfoResponseDto);
        UserMetersDataResponseDto result = userService.getUserMetersData("2018-06-01", "2018-06-30");

        assertEquals(userMetersDataResponseDto, result);
    }


    @Test
    public void testNoData() throws CmdException {
        LocalDate fromDateL = LocalDate.of(2018, 6, 1);
        LocalDate toDateL = LocalDate.of(2018, 6, 30);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> dataList = new ArrayList<>();

        when(dataDao.getUserData(fromDateL, toDateL, user.getId())).thenReturn(dataList);

        try {
            userService.getUserMetersData("2018-06-01", "2018-06-30");
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATA_IS_EMPTY, ex.getErrorCodes());
        }

    }

    @Test
    public void testAddMeterData() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("20", "20", "20", "2018-06-28");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);

        Map<String, MetersDataResponse> metersData = new HashMap<>();
        metersData.put("2018-06-12", new MetersDataResponse(10, 10, 10));
        metersData.put("2018-06-20", new MetersDataResponse(15, 15, 15));
        metersData.put("2018-06-28", new MetersDataResponse(20, 20, 20));

        UserInfoResponseDto userInfoResponseDto = new UserInfoResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic());
        AddUserDataResponseDto myResponse = new AddUserDataResponseDto(metersData, userInfoResponseDto);

        AddUserDataResponseDto result = userService.addUserData(userAddDataRequestDto);

        assertEquals(myResponse, result);
    }


    @Test
    public void testAddMeterSecondTime() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("20", "20", "20", "2018-06-20");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);


        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATA_EXIST_THIS_DAY, ex.getErrorCodes());
        }
    }


    @Test
    public void testAddMeterSmallData() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("12", "12", "12", "2018-06-28");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);

        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATA_INCORRECT, ex.getErrorCodes());
        }
    }


    @Test
    public void testAddMeterDataIncorrectData() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("abcdef", "ghjklmnop", "qrstuvwxyz", "2018-06-28");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);
        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATA_WRONG, ex.getErrorCodes());
        }
    }

    @Test
    public void testAddMeterDataNullDate() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("10", "10", "10", null);

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);
        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATE_IS_EMPTY, ex.getErrorCodes());
        }
    }


    @Test
    public void testAddMeterDataIncorrectDate() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("10", "10", "10", "123456789");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);
        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATE_WRONG, ex.getErrorCodes());
        }
    }

    @Test
    public void testAddMeterDataEmptyDate() throws CmdException {
        Set<Data> data = new HashSet<>();
        data.add(new Data(null, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        data.add(new Data(null, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), data);
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        UserAddDataRequestDto userAddDataRequestDto = new UserAddDataRequestDto("10", "10", "10", "");

        Data insertData = new Data(user, 20, 20, 20, LocalDate.of(2018, 6, 28));
        when(dataDao.addUserData(insertData)).thenReturn(insertData);
        try {
            userService.addUserData(userAddDataRequestDto);
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATE_IS_EMPTY, ex.getErrorCodes());
        }
    }

    @Test
    public void testGetDataIncorrectDate() throws CmdException {
        LocalDate fromDateL = LocalDate.of(2018, 6, 1);
        LocalDate toDateL = LocalDate.of(2018, 6, 30);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> dataList = new ArrayList<>();
        dataList.add(new Data(user, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        dataList.add(new Data(user, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        when(dataDao.getUserData(fromDateL, toDateL, user.getId())).thenReturn(dataList);

        try {
            UserMetersDataResponseDto result = userService.getUserMetersData("ABCDEFG", "ABCDEFG");
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.DATE_WRONG, ex.getErrorCodes());
        }
    }

    @Test
    public void testFirstDateAfterSecondDate() throws CmdException {
        LocalDate fromDateL = LocalDate.of(2018, 6, 1);
        LocalDate toDateL = LocalDate.of(2018, 6, 30);

        User user = new User("Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> dataList = new ArrayList<>();
        dataList.add(new Data(user, 10, 10, 10, LocalDate.of(2018, 6, 12)));
        dataList.add(new Data(user, 15, 15, 15, LocalDate.of(2018, 6, 20)));

        when(dataDao.getUserData(fromDateL, toDateL, user.getId())).thenReturn(dataList);

        try {
            UserMetersDataResponseDto result = userService.getUserMetersData("2018-07-01", "2018-06-01");
        } catch (CmdException ex) {
            assertEquals(ErrorCodes.START_DATE_LONGER_END_DATE, ex.getErrorCodes());
        }
    }



    @Test
    public void testGetUserWithForecast() throws CmdException {
        LocalDate fromDateL = LocalDate.of(2018, 7, 1);
        LocalDate toDateL = LocalDate.of(2018, 7, 4);

        User user = new User(1, "Иван", "Иванов", "Иванович", "ROLE_ADMIN", "admin", "admin", "ivanov@gmail.com", "88005553535", "5", new Location(), new HashSet<>());
        when(userDao.getUserByLogin(baseService.getUserName())).thenReturn(user);

        List<Data> dataList = new ArrayList<>();
        dataList.add(new Data(user, 10, 10, 10, LocalDate.of(2018, 7, 1)));
        dataList.add(new Data(user, 20, 20, 20, LocalDate.of(2018, 7, 2)));
        dataList.add(new Data(user, 30, 30, 30, LocalDate.of(2018, 7, 3)));
        dataList.add(new Data(user, 40, 40, 40, LocalDate.of(2018, 7, 4)));

        when(dataDao.getUserData(eq(fromDateL), eq(toDateL), eq(user.getId()))).thenReturn(dataList);

        Map<String, MetersDataResponse> responseData = new LinkedHashMap<>();
        responseData.put("2018-07-01", new MetersDataResponse(10, 10, 10));
        responseData.put("2018-07-02", new MetersDataResponse(20, 20, 20));
        responseData.put("2018-07-03", new MetersDataResponse(30, 30, 30));
        responseData.put("2018-07-04", new MetersDataResponse(40, 40, 40));

        Map<String, PredictionalMetersDataResponseDto> predictionalData = new LinkedHashMap<>();
        predictionalData.put("2018-07-05", new PredictionalMetersDataResponseDto(48, 48,48));
        predictionalData.put("2018-07-06", new PredictionalMetersDataResponseDto(58, 58,58));

        UserInfoResponseDto userInfoResponseDto = new UserInfoResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic());

        UserMetersDataResponseDto userMetersDataResponseDto = new UserMetersDataResponseDto(responseData, predictionalData, userInfoResponseDto);
        UserMetersDataResponseDto result = userService.getUserMetersData("2018-07-01", "2018-07-04");

        assertEquals(userMetersDataResponseDto.getMetersData(), result.getMetersData());
        assertEquals(userMetersDataResponseDto.getPredictionalMetersData(), result.getPredictionalMetersData());
        assertEquals(userMetersDataResponseDto.getUserInfo(), result.getUserInfo());
    }


}
