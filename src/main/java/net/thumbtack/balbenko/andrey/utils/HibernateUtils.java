package net.thumbtack.balbenko.andrey.utils;

import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.Location;
import net.thumbtack.balbenko.andrey.domain.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    public static SessionFactory buildSessionFactory() {
        try {
            Configuration conf = new Configuration();
            conf.addResource("hibernate.cfg.xml");
            conf.addAnnotatedClass(Location.class);
            conf.addAnnotatedClass(User.class);
            conf.addAnnotatedClass(Data.class);
            return conf.configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
