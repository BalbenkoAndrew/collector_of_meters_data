package net.thumbtack.balbenko.andrey.config.swagger;

import net.thumbtack.balbenko.andrey.controllers.AdminController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static com.google.common.collect.Lists.*;

@Configuration
@EnableSwagger2
@ComponentScan(basePackageClasses = AdminController.class)
@PropertySource("classpath:swagger.properties")
public class SwaggerConfig {

    private static final String SWAGGER_API_VERSION = "1.0";
    private static final String LICENSE_NEXT = "License";
    private static final String TITLE = "CMD REST API";
    private static final String DESCRIPTION = "RESTful API for CMD";

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(TITLE)
                .license(LICENSE_NEXT)
                .description(DESCRIPTION)
                .version(SWAGGER_API_VERSION)
                .build();
    }

    @Bean
    public Docket cmdApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .pathMapping("/")
                .select().build()
                .securitySchemes(newArrayList(basicAuth()));
    }



    private BasicAuth basicAuth() {
        return new BasicAuth("auth");
    }
}
