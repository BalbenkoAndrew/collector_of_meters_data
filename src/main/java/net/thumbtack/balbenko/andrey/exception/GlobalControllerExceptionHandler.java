package net.thumbtack.balbenko.andrey.exception;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.FailureResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@ControllerAdvice
public class GlobalControllerExceptionHandler implements ResponseErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CmdException.class)
    @ResponseBody
    public ResponseEntity<FailureResponse> handleCmdException(CmdException ex) {
        FailureResponse failureResponse = new FailureResponse(ex.getErrorCodes().toString(), ex.getErrorCodes().getMessage());
        return new ResponseEntity<>(failureResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

    }
}
