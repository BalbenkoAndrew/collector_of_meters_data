package net.thumbtack.balbenko.andrey.exception;

public class CmdException extends Exception {
    private ErrorCodes errorCodes;

    public CmdException(ErrorCodes errorCodes) {
        this.errorCodes = errorCodes;
    }

    public ErrorCodes getErrorCodes() {
        return errorCodes;
    }
}
