package net.thumbtack.balbenko.andrey.exception;

public enum ErrorCodes {
    DATA_EXIST_THIS_DAY("Данные за этот день уже переданы"),
    DATA_INCORRECT("Данные, которые вы пытаетесь передать, меньше данных, которые были при последней передаче"),
    DATA_WRONG("Данные введены некорректно"),
    DATE_LESS_CURRENT_DATE("Дата, которую вы хотите передать меньше даты последней передачи данных"),
    START_DATE_LONGER_END_DATE("Дата начала не должна привышать даты окончания!"),
    DATA_IS_EMPTY("Данные в этот период времени не передавались"),
    DATE_WRONG("Введена некорректная дата!"),
    DATE_IS_EMPTY("Дата не введена!");


    private String message;

    private ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
