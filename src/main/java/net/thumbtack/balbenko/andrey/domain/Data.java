package net.thumbtack.balbenko.andrey.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="data")
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Column(name = "hot")
    private int hot;

    @Column(name = "cold")
    private int cold;

    @Column(name = "electricity")
    private int electricity;

    @Column(name = "date")
    @JsonIgnore
    private LocalDate date;

    public Data(int id, User user, int hot, int cold, int electricity, LocalDate date) {
        this.id = id;
        this.user = user;
        this.hot = hot;
        this.cold = cold;
        this.electricity = electricity;
        this.date = date;
    }

    public Data(User user, int hot, int cold, int electricity, LocalDate date) {
        this(0, user, hot, cold, electricity, date);
    }

    public Data() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }

    public int getCold() {
        return cold;
    }

    public void setCold(int cold) {
        this.cold = cold;
    }

    public int getElectricity() {
        return electricity;
    }

    public void setElectricity(int electricity) {
        this.electricity = electricity;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return id == data.id &&
                hot == data.hot &&
                cold == data.cold &&
                electricity == data.electricity &&
                Objects.equals(user, data.user) &&
                Objects.equals(date, data.date);
    }
}
