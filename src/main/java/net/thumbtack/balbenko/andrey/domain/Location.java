package net.thumbtack.balbenko.andrey.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    @Column(name = "street")
    private String street;

    @Column(name = "house")
    private int house;

    @Column(name = "housing")
    private String housing;

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JsonIgnore
    private Set<User> users;

    public Location(int id, String street, int house, String housing, Set<User> users) {
        this.id = id;
        this.street = street;
        this.house = house;
        this.housing = housing;
        this.users = users;
    }

    public Location(String street, int house, String housing, Set<User> users) {
        this(0, street, house, housing, users);
    }

    public Location() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouse() {
        return house;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    public String getHousing() {
        return housing;
    }

    public void setHousing(String housing) {
        this.housing = housing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return id == location.id &&
                house == location.house &&
                Objects.equals(street, location.street) &&
                Objects.equals(housing, location.housing) &&
                Objects.equals(users, location.users);
    }
}
