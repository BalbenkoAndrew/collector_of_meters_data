package net.thumbtack.balbenko.andrey.domain;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "role")
    private String role;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "room")
    private String room;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Data> data;

    public User(int id, String firstName, String lastName, String patronymic, String role, String login,
                String password, String email, String phone, String room, Location location, Set<Data> data) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.role = role;
        this.login = login;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.room = room;
        this.location = location;
        this.data = data;
    }

    public User(String firstName, String lastName, String patronymic, String role, String login,
                String password, String email, String phone, String room, Location location, Set<Data> data) {
        this(0, firstName, lastName, patronymic, role, login, password, email, phone, room, location, data);
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getRole() {
        return role;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getRoom() {
        return room;
    }

    public Location getLocation() {
        return location;
    }

    public String getLastname() {
        return lastName;
    }

    public Set<Data> getData() {
        return data;
    }


}
