package net.thumbtack.balbenko.andrey.service;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.PredictionalMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.daoimpl.DataDaoImpl;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import net.thumbtack.balbenko.andrey.utils.Sender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class BaseService {
    private DataDao dataDao = new DataDaoImpl();
    private static final double COEFFICIENT_SMOOTHING_SERIES = 0.9;
    private static final double COEFFICIENT_TREND_SMOOTHING = 0.9;

    public String getUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User securityUser = (org.springframework.security.core.userdetails.User)
                auth.getPrincipal();
        return securityUser.getUsername();
    }

    public static Map<String, PredictionalMetersDataResponseDto> predictionAlgorithm(List<Data> data) {
        LocalDate startDate = data.get(0).getDate();
        LocalDate endDate = data.get(data.size() - 1).getDate();

        Period p = Period.between(startDate, endDate);

        if ((p.getDays() + 1) != data.size()) {
            return null;
        }

        List<Double> exponentiallySmoothedSeriesColdWater = new ArrayList<>();
        exponentiallySmoothedSeriesColdWater.add((double)data.get(0).getCold());

        List<Double> exponentiallySmoothedSeriesHotWater = new ArrayList<>();
        exponentiallySmoothedSeriesHotWater.add((double)data.get(0).getHot());

        List<Double> exponentiallySmoothedSeriesElectricity = new ArrayList<>();
        exponentiallySmoothedSeriesElectricity.add((double)data.get(0).getElectricity());

        double smoothedSeriesFirst = 0.0;

        List<Double> trendColdWater = new ArrayList<>();
        trendColdWater.add(smoothedSeriesFirst);

        List<Double> trendHotWater = new ArrayList<>();
        trendHotWater.add(smoothedSeriesFirst);

        List<Double> trendElectricity = new ArrayList<>();
        trendElectricity.add(smoothedSeriesFirst);


        for (int i = 1; i < data.size(); i++) {
            Double exponentialSmoothSeriesColdWater = (COEFFICIENT_SMOOTHING_SERIES * data.get(i).getCold() + (1 - COEFFICIENT_SMOOTHING_SERIES) * (exponentiallySmoothedSeriesColdWater.get(i - 1) - trendColdWater.get(i - 1)));
            exponentiallySmoothedSeriesColdWater.add(exponentialSmoothSeriesColdWater);

            Double trendCold = (COEFFICIENT_TREND_SMOOTHING * (exponentiallySmoothedSeriesColdWater.get(i) - exponentiallySmoothedSeriesColdWater.get(i - 1)) + (1 - COEFFICIENT_TREND_SMOOTHING) * trendColdWater.get(i - 1));
            trendColdWater.add(trendCold);



            Double exponentialSmoothSeriesHotWater = (COEFFICIENT_SMOOTHING_SERIES * data.get(i).getHot() + (1 - COEFFICIENT_SMOOTHING_SERIES) * (exponentiallySmoothedSeriesHotWater.get(i - 1) - trendHotWater.get(i - 1)));
            exponentiallySmoothedSeriesHotWater.add(exponentialSmoothSeriesHotWater);

            Double trendHot = (COEFFICIENT_TREND_SMOOTHING * (exponentiallySmoothedSeriesHotWater.get(i) - exponentiallySmoothedSeriesHotWater.get(i - 1)) + (1 - COEFFICIENT_TREND_SMOOTHING) * trendHotWater.get(i - 1));
            trendHotWater.add(trendHot);



            Double exponentialSmoothSeriesElectricity = (COEFFICIENT_SMOOTHING_SERIES * data.get(i).getElectricity() + (1 - COEFFICIENT_SMOOTHING_SERIES) * (exponentiallySmoothedSeriesElectricity.get(i - 1) - trendElectricity.get(i - 1)));
            exponentiallySmoothedSeriesElectricity.add(exponentialSmoothSeriesElectricity);

            Double trendElectr = (COEFFICIENT_TREND_SMOOTHING * (exponentiallySmoothedSeriesElectricity.get(i) - exponentiallySmoothedSeriesElectricity.get(i - 1)) + (1 - COEFFICIENT_TREND_SMOOTHING) * trendElectricity.get(i - 1));
            trendElectricity.add(trendElectr);
        }

        Map<String, PredictionalMetersDataResponseDto> predictedData = new LinkedHashMap<>();

        int period = 1;
        LocalDate lastDate = data.get(data.size() - 1).getDate();
        for (int i = 0; i < data.size() / 2; i++) {
            PredictionalMetersDataResponseDto dataOneDay = new PredictionalMetersDataResponseDto();
            int predictionalColdWater = (int)Math.ceil(exponentiallySmoothedSeriesColdWater.get(exponentiallySmoothedSeriesColdWater.size() - 1) + period * trendColdWater.get(trendColdWater.size() - 1));
            dataOneDay.setCold_w(predictionalColdWater);

            int predictionalHotWater = (int)Math.ceil(exponentiallySmoothedSeriesHotWater.get(exponentiallySmoothedSeriesHotWater.size() - 1) + period * trendHotWater.get(trendHotWater.size() - 1));
            dataOneDay.setHot_w(predictionalHotWater);

            int predictionalElectricity = (int)Math.ceil(exponentiallySmoothedSeriesElectricity.get(exponentiallySmoothedSeriesElectricity.size() - 1) + period * trendElectricity.get(trendElectricity.size() - 1));
            dataOneDay.setElectricity(predictionalElectricity);

            LocalDate nowDate = lastDate.plusDays(i + 1);
            predictedData.put(nowDate.toString(), dataOneDay);
            period++;
        }

        return predictedData;
    }


    public List<Data> getData(String fromDate, String toDate, int userId) throws CmdException {
        LocalDate fromDateL = null;
        LocalDate toDateL = null;
        try {
            fromDateL = LocalDate.parse(fromDate);
            toDateL = LocalDate.parse(toDate);
        } catch (ClassCastException ex) {
            throw new CmdException(ErrorCodes.DATE_WRONG);
        }

        if (fromDateL.isAfter(toDateL)) {
            throw new CmdException(ErrorCodes.START_DATE_LONGER_END_DATE);
        }

        return dataDao.getUserData(fromDateL, toDateL, userId);
    }


    public static void sendMessageToGmail(String toEmail, String message) {
        Sender sender = new Sender("cmdlineate@gmail.com", "cmdpassword");
        sender.send("Показания", message, "cmdlineate@gmail.com", toEmail);
    }

}
