package net.thumbtack.balbenko.andrey.service;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.AdminGetDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.LocationDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdminService extends BaseService {
    private Logger LOGGER = LoggerFactory.getLogger(AdminService.class);
    private LocationDao locationDao;
    private DataDao dataDao;
    private UserDao userDao;

    public AdminService(LocationDao locationDao, DataDao dataDao, UserDao userDao) {
        this.locationDao = locationDao;
        this.dataDao = dataDao;
        this.userDao = userDao;
    }

    public AdminGetDataResponseDto getMetersDataByMonth(String date) throws CmdException {
        LocalDate nowDate = null;
        try {
            nowDate = LocalDate.parse(date);
        } catch (DateTimeParseException ex) {
            LOGGER.info("Invalid date entered: " + date);
            throw new CmdException(ErrorCodes.DATE_WRONG);
        }
        LocalDate fromDate = nowDate.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate toDate = nowDate.with(TemporalAdjusters.lastDayOfMonth());

        User user = userDao.getUserByLogin(getUserName());

        List<Data> dataByMonth = dataDao.getDataByMonth(fromDate, toDate);

        if (dataByMonth.isEmpty()) {
            LOGGER.info("Data for " + date + " does not exist");
            throw new CmdException(ErrorCodes.DATA_IS_EMPTY);
        }

        List<Data> sortedData = dataByMonth.stream().filter(x -> x.getUser().getLocation().getId() == (user.getLocation().getId())).sorted(Comparator.comparing(o -> o.getUser().getRoom())).collect(Collectors.toList());

        Map<Integer, Map<String, MetersDataResponse>> dataByRoomAndDate = new HashMap<>();
        Map<String, MetersDataResponse> oneRoomsData = new HashMap<>();
        for (int i = 0; i < sortedData.size(); i++) {
            if (oneRoomsData == null) {
                oneRoomsData = new HashMap<>();
            }
            if ((i + 1) == sortedData.size()) {
                oneRoomsData.put(sortedData.get(i).getDate().toString(), new MetersDataResponse(sortedData.get(i).getHot(), sortedData.get(i).getCold(), sortedData.get(i).getElectricity()));
                dataByRoomAndDate.put(Integer.parseInt(sortedData.get(i).getUser().getRoom()), oneRoomsData);
                break;
            }
            String room = sortedData.get(i + 1).getUser().getRoom();
            if (!sortedData.get(i).getUser().getRoom().equals(room)) {
                oneRoomsData.put(sortedData.get(i).getDate().toString(), new MetersDataResponse(sortedData.get(i).getHot(), sortedData.get(i).getCold(), sortedData.get(i).getElectricity()));
                dataByRoomAndDate.put(Integer.parseInt(sortedData.get(i).getUser().getRoom()), oneRoomsData);
                oneRoomsData = null;
            } else {
                oneRoomsData.put(sortedData.get(i).getDate().toString(), new MetersDataResponse(sortedData.get(i).getHot(), sortedData.get(i).getCold(), sortedData.get(i).getElectricity()));
            }
        }

        Set<Integer> rooms = dataByRoomAndDate.keySet();
        Map<Integer, MetersDataResponse> dataByRoomsResponse = new HashMap<>();

        for (Integer i : rooms) {
            Map<String, MetersDataResponse> metersDataByDate = dataByRoomAndDate.get(i);
            Set<String> datesByRoom = metersDataByDate.keySet();
            MetersDataResponse metersDataResponse = new MetersDataResponse();
            for (String s : datesByRoom) {
                metersDataResponse.setCold_w(metersDataResponse.getCold_w() + metersDataByDate.get(s).getCold_w());
                metersDataResponse.setHot_w(metersDataResponse.getHot_w() + metersDataByDate.get(s).getHot_w());
                metersDataResponse.setElectricity(metersDataResponse.getElectricity() + metersDataByDate.get(s).getElectricity());
            }

            metersDataResponse.setCold_w(metersDataResponse.getCold_w() / datesByRoom.size());
            metersDataResponse.setHot_w(metersDataResponse.getHot_w() / datesByRoom.size());
            metersDataResponse.setElectricity(metersDataResponse.getElectricity() / datesByRoom.size());

            dataByRoomsResponse.put(i, metersDataResponse);
        }

        return new AdminGetDataResponseDto(dataByRoomsResponse, date);
    }
}
