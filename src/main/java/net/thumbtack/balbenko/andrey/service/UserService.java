package net.thumbtack.balbenko.andrey.service;

import net.thumbtack.balbenko.andrey.controllers.dto.request.EditUserDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.request.UserAddDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.AddUserDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.PersonalDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.UserMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.PredictionalMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.UserInfoResponseDto;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;

import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.exception.ErrorCodes;
import net.thumbtack.balbenko.andrey.domain.Data;
import net.thumbtack.balbenko.andrey.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService extends BaseService {
    private Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private UserDao userDao;
    private DataDao dataDao;

    public UserService(UserDao userDao, DataDao dataDao) {
        this.userDao = userDao;
        this.dataDao = dataDao;
    }

    public UserMetersDataResponseDto getUserMetersData(String fromDate, String toDate) throws CmdException {
        User user = userDao.getUserByLogin(getUserName());

        LocalDate fromDateL = null;
        LocalDate toDateL = null;
        try {
            fromDateL = LocalDate.parse(fromDate);
            toDateL = LocalDate.parse(toDate);
        } catch (DateTimeParseException ex) {
            LOGGER.info("User: " + user + " enter incorrect date: fromDate: " + fromDate + " and toDate: " + toDate, ex);
            throw new CmdException(ErrorCodes.DATE_WRONG);
        }

        if (fromDateL.isAfter(toDateL)) {
            LOGGER.info("User: " + user + " entered the start date more than the end date");
            throw new CmdException(ErrorCodes.START_DATE_LONGER_END_DATE);
        }

        List<Data> dataList = dataDao.getUserData(fromDateL, toDateL, user.getId());
        if (dataList.isEmpty()) {
            LOGGER.info("Data for the period: " + fromDate + " - " + toDate + "does not exist");
            throw new CmdException(ErrorCodes.DATA_IS_EMPTY);
        }

        dataList.sort(Comparator.comparing(Data::getDate));

        Map<String, PredictionalMetersDataResponseDto> predictedData = null;

        Data lastData = dataDao.getUserDataByDateToDate(toDateL, user.getId());
        if (lastData == null) {
            predictedData = predictionAlgorithm(dataList);
        }

        Map<String, MetersDataResponse> responseData = new LinkedHashMap<>();

        for (Data data : dataList) {
            responseData.put(data.getDate().toString(), new MetersDataResponse(data.getHot(), data.getCold(), data.getElectricity()));
        }

        UserInfoResponseDto userInfo = new UserInfoResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic());

        return new UserMetersDataResponseDto(responseData, predictedData, userInfo);
    }


    public AddUserDataResponseDto addUserData(UserAddDataRequestDto userAddDataRequestDto) throws CmdException {
        User user = userDao.getUserByLogin(getUserName());

        int cold;
        int hot;
        int electricity;

        try {
            cold = Integer.parseInt(userAddDataRequestDto.getColdWater());
            hot = Integer.parseInt(userAddDataRequestDto.getHotWater());
            electricity = Integer.parseInt(userAddDataRequestDto.getElectricity());
        } catch (NumberFormatException ex) {
            LOGGER.info("Incorrect data entered");
            throw new CmdException(ErrorCodes.DATA_WRONG);
        }

        if (cold <= 0 || hot <= 0 || electricity <= 0) {
            LOGGER.info("Data: " + userAddDataRequestDto + "incorrect");
            throw new CmdException(ErrorCodes.DATA_INCORRECT);
        }


        if (userAddDataRequestDto.getDate() == null || userAddDataRequestDto.getDate().isEmpty()) {
            LOGGER.info("Date: " + userAddDataRequestDto.getDate() + " is empty on equals null");
            throw new CmdException(ErrorCodes.DATE_IS_EMPTY);
        }

        List<LocalDate> dataList = user.getData().stream().map(Data::getDate).sorted().collect(Collectors.toList());

        if (!dataList.isEmpty()) {
            LocalDate lastDate = dataList.get(dataList.size() - 1);
            LocalDate nowDate;
            try {
                nowDate = LocalDate.parse(userAddDataRequestDto.getDate());
            } catch (DateTimeParseException ex) {
                LOGGER.info("Invalid date entered: " + userAddDataRequestDto.getDate());
                throw new CmdException(ErrorCodes.DATE_WRONG);
            }
            if ((lastDate.isAfter(nowDate))) {
                LOGGER.info("Date: " + userAddDataRequestDto.getDate() + " is less than the date of the last data transfer");
                throw new CmdException(ErrorCodes.DATE_LESS_CURRENT_DATE);
            }

            if (lastDate.getMonth().equals(nowDate.getMonth()) && lastDate.getYear() == nowDate.getYear() && lastDate.getDayOfMonth() == nowDate.getDayOfMonth()) {
                LOGGER.info("Data for this day: " + userAddDataRequestDto.getDate() + " is still transferred");
                throw new CmdException(ErrorCodes.DATA_EXIST_THIS_DAY);
            }

            Data data = user.getData().stream().filter(data1 -> Objects.equals(data1.getDate(), lastDate)).findFirst().get();

            if (data.getCold() >= cold || data.getHot() >= hot || data.getElectricity() >= electricity) {
                LOGGER.info("The data: " + userAddDataRequestDto + " to transfer is less than the data that was transmitted in the last transmission");
                throw new CmdException(ErrorCodes.DATA_INCORRECT);
            }
        }

        Data data = new Data(user, hot, cold, electricity, LocalDate.parse(userAddDataRequestDto.getDate()));

        Data insertData = dataDao.addUserData(data);


        String message = "Следующие данные были переданы " + userAddDataRequestDto.getDate() + " : \n"
                + "   Горячая вода: " + hot + "\n   Холодная вода: " + cold + "\n   Электроэнергия: " + electricity;
        sendMessageToGmail(user.getEmail(), message);


        List<Data> allUsersData = new ArrayList<>();
        allUsersData.addAll(user.getData());
        allUsersData.add(insertData);

        Collections.sort(allUsersData, Comparator.comparing(Data::getDate));


        Map<String, MetersDataResponse> map = new TreeMap<>();
        for (Data d : allUsersData) {
            map.put(d.getDate().toString(), new MetersDataResponse(d.getHot(), d.getCold(), d.getElectricity()));
        }

        return new AddUserDataResponseDto(map, new UserInfoResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic()));
    }

    public void sendMessage(String fromDate, String toDate) throws CmdException {
        User user = userDao.getUserByLogin(getUserName());
        List<Data> data = getData(fromDate, toDate, user.getId());
        data.sort(Comparator.comparing(Data::getDate));
        sendMessageToGmail(user.getEmail(), createMessageGetData(data));
    }

    public String createMessageGetData(List<Data> data) {
        StringBuilder message = new StringBuilder("Данные: \n");
        for (int i = 0; i < data.size(); i++) {
            message.append(data.get(i).getDate().toString()).append("\n");
            message.append("   Горячая: ").append(data.get(i).getHot()).append("\n");
            message.append("   Холодная: ").append(data.get(i).getCold()).append("\n");
            message.append("   Электроэнергия: ").append(data.get(i).getElectricity()).append("\n");
        }
        return message.toString();
    }

    public PersonalDataResponseDto getPersonalData() {
        User user = userDao.getUserByLogin(getUserName());
        return new PersonalDataResponseDto(user.getFirstName(), user.getLastname(), user.getPatronymic(),
                user.getEmail(), user.getPhone(), user.getLocation().getStreet(), String.valueOf(user.getLocation().getHouse()), user.getRoom());
    }


    public void editUserData(EditUserDataRequestDto editUserDataRequestDto) throws CmdException {
        User user = userDao.getUserByLogin(getUserName());

        int cold;
        int hot;
        int electricity;
        try {
            cold = Integer.parseInt(editUserDataRequestDto.getCold());
            hot = Integer.parseInt(editUserDataRequestDto.getHot());
            electricity = Integer.parseInt(editUserDataRequestDto.getElectricity());
        } catch (NumberFormatException ex) {
            throw new CmdException(ErrorCodes.DATA_WRONG);
        }

        LocalDate nowDate = null;
        try {
            nowDate = LocalDate.parse(editUserDataRequestDto.getDate());
        } catch (DateTimeParseException ex) {
            throw new CmdException(ErrorCodes.DATE_WRONG);
        }

        Data userDataByDate = dataDao.getUserDataByDate(nowDate, user.getId());
        Data data = new Data(userDataByDate.getId(), user, hot, cold, electricity, nowDate);
        Data fromData = dataDao.getUserDataByDateFromDate(nowDate, user.getId());
        Data toData = dataDao.getUserDataByDateToDate(nowDate, user.getId());

        if (fromData != null) {
            if (data.getHot() < fromData.getHot() || data.getCold() < fromData.getCold() || data.getElectricity() < fromData.getElectricity()) {
                throw new CmdException(ErrorCodes.DATA_WRONG);
            }
        }
        if (toData != null) {
            if (data.getHot() > toData.getHot() || data.getCold() > toData.getCold() || data.getElectricity() > toData.getElectricity()) {
                throw new CmdException(ErrorCodes.DATA_WRONG);
            }
        }

        dataDao.editUserData(data);
    }
}

