package net.thumbtack.balbenko.andrey.controllers.dto.responce.internal;

import java.util.Objects;

public class MetersDataResponse {
    private int hot_w;
    private int cold_w;
    private int electricity;

    public MetersDataResponse(int hot_w, int cold_w, int electricity) {
        this.hot_w = hot_w;
        this.cold_w = cold_w;
        this.electricity = electricity;
    }

    public MetersDataResponse() {
    }

    public int getHot_w() {
        return hot_w;
    }

    public void setHot_w(int hot_w) {
        this.hot_w = hot_w;
    }

    public int getCold_w() {
        return cold_w;
    }

    public void setCold_w(int cold_w) {
        this.cold_w = cold_w;
    }

    public int getElectricity() {
        return electricity;
    }

    public void setElectricity(int electricity) {
        this.electricity = electricity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetersDataResponse that = (MetersDataResponse) o;
        return hot_w == that.hot_w &&
                cold_w == that.cold_w &&
                electricity == that.electricity;
    }

    @Override
    public int hashCode() {

        return Objects.hash(hot_w, cold_w, electricity);
    }
}
