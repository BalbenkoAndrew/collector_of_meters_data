package net.thumbtack.balbenko.andrey.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.AdminGetDataResponseDto;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.service.AdminService;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
    private AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(value = "/admin/data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Admin getting meters data by month")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = AdminGetDataResponseDto.class)})
    @Secured("ROLE_ADMIN")
    public AdminGetDataResponseDto getMetersDataByMonth(@RequestParam(value = "date", required = false) String date) throws CmdException {
        return adminService.getMetersDataByMonth(date);
    }
}
