package net.thumbtack.balbenko.andrey.controllers.dto.responce;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.UserInfoResponseDto;

import java.util.Map;
import java.util.Objects;

public class AddUserDataResponseDto {
    private Map<String, MetersDataResponse> metersData;
    private UserInfoResponseDto userInfo;

    public AddUserDataResponseDto() {
    }

    public AddUserDataResponseDto(Map<String, MetersDataResponse> metersData, UserInfoResponseDto userInfo) {
        this.metersData = metersData;
        this.userInfo = userInfo;
    }

    public Map<String, MetersDataResponse> getMetersData() {
        return metersData;
    }

    public UserInfoResponseDto getUserInfo() {
        return userInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddUserDataResponseDto that = (AddUserDataResponseDto) o;
        return Objects.equals(metersData, that.metersData) &&
                Objects.equals(userInfo, that.userInfo);
    }
}
