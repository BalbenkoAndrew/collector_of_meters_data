package net.thumbtack.balbenko.andrey.controllers.dto.request;

import java.util.Objects;

public class EditUserDataRequestDto {
    private String hot;
    private String cold;
    private String electricity;
    private String date;
    private String fromDate;
    private String toDate;

    public EditUserDataRequestDto() {
    }

    public EditUserDataRequestDto(String hot, String cold, String electricity, String date, String fromDate, String toDate) {
        this.hot = hot;
        this.cold = cold;
        this.electricity = electricity;
        this.date = date;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getCold() {
        return cold;
    }

    public void setCold(String cold) {
        this.cold = cold;
    }

    public String getElectricity() {
        return electricity;
    }

    public void setElectricity(String electricity) {
        this.electricity = electricity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EditUserDataRequestDto that = (EditUserDataRequestDto) o;
        return Objects.equals(hot, that.hot) &&
                Objects.equals(cold, that.cold) &&
                Objects.equals(electricity, that.electricity) &&
                Objects.equals(date, that.date) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(hot, cold, electricity, date, fromDate, toDate);
    }
}
