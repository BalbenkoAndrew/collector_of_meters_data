package net.thumbtack.balbenko.andrey.controllers.dto.responce;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;

import java.util.Map;
import java.util.Objects;

public class AdminGetDataResponseDto {
    private Map<Integer, MetersDataResponse> roomResponse;
    private String date;

    public AdminGetDataResponseDto(Map<Integer, MetersDataResponse> roomResponse, String date) {
        this.roomResponse = roomResponse;
        this.date = date;
    }

    public AdminGetDataResponseDto() {
    }

    public Map<Integer, MetersDataResponse> getRoomResponse() {
        return roomResponse;
    }

    public void setRoomResponse(Map<Integer, MetersDataResponse> roomResponse) {
        this.roomResponse = roomResponse;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdminGetDataResponseDto that = (AdminGetDataResponseDto) o;
        return Objects.equals(roomResponse, that.roomResponse) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roomResponse, date);
    }
}
