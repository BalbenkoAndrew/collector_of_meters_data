package net.thumbtack.balbenko.andrey.controllers.dto.responce;

import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.MetersDataResponse;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.PredictionalMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.internal.UserInfoResponseDto;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UserMetersDataResponseDto {
    private Map<String, MetersDataResponse> metersData;
    private Map<String, PredictionalMetersDataResponseDto> predictionalMetersData;
    private UserInfoResponseDto userInfo;


    public UserMetersDataResponseDto(Map<String, MetersDataResponse> metersData, Map<String, PredictionalMetersDataResponseDto> predictionalMetersData, UserInfoResponseDto userInfo) {
        this.metersData = metersData;
        this.predictionalMetersData = predictionalMetersData;
        this.userInfo = userInfo;
    }

    public UserMetersDataResponseDto() {
    }

    public Map<String, PredictionalMetersDataResponseDto> getPredictionalMetersData() {
        return predictionalMetersData;
    }

    public void setPredictionalMetersData(Map<String, PredictionalMetersDataResponseDto> predictionalMetersData) {
        this.predictionalMetersData = predictionalMetersData;
    }

    public Map<String, MetersDataResponse> getMetersData() {
        return metersData;
    }

    public void setMetersData(Map<String, MetersDataResponse> metersData) {
        this.metersData = metersData;
    }

    public UserInfoResponseDto getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoResponseDto userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMetersDataResponseDto that = (UserMetersDataResponseDto) o;
        return Objects.equals(metersData, that.metersData) &&
                Objects.equals(predictionalMetersData, that.predictionalMetersData) &&
                Objects.equals(userInfo, that.userInfo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(metersData, predictionalMetersData, userInfo);
    }
}
