package net.thumbtack.balbenko.andrey.controllers.dto.request;

import io.swagger.annotations.ApiModelProperty;

public class UserAddDataRequestDto {
    @ApiModelProperty(example = "15")
    private String hotWater;
    @ApiModelProperty(example = "15")
    private String coldWater;
    @ApiModelProperty(example = "15")
    private String electricity;
    @ApiModelProperty(example = "15")
    private String date;

    public UserAddDataRequestDto() {
    }

    public UserAddDataRequestDto(String hotWater, String coldWater, String electricity, String date) {
        this.hotWater = hotWater;
        this.coldWater = coldWater;
        this.electricity = electricity;
        this.date = date;
    }

    public String getHotWater() {
        return hotWater;
    }

    public String getColdWater() {
        return coldWater;
    }

    public String getElectricity() {
        return electricity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
