package net.thumbtack.balbenko.andrey.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.thumbtack.balbenko.andrey.controllers.dto.request.EditUserDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.request.UserAddDataRequestDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.AddUserDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.PersonalDataResponseDto;
import net.thumbtack.balbenko.andrey.controllers.dto.responce.UserMetersDataResponseDto;
import net.thumbtack.balbenko.andrey.exception.CmdException;
import net.thumbtack.balbenko.andrey.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/user/data/get", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("User get meters data")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = UserMetersDataResponseDto.class)})
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public UserMetersDataResponseDto getMetersData(@RequestParam(value = "fromDate", required = false) String fromDate,
                                                   @RequestParam(value = "toDate", required = false) String toDate) throws CmdException {
        return userService.getUserMetersData(fromDate, toDate);
    }

    @PostMapping(value = "/user/data/add")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = AddUserDataResponseDto.class)})
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public AddUserDataResponseDto addUserData(@RequestBody UserAddDataRequestDto userAddDataRequestDto) throws Exception {
        return userService.addUserData(userAddDataRequestDto);
    }

    @PostMapping(value = "/user/data/send")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public void sendMessageToEmail(@RequestParam(value = "fromDate", required = false) String fromDate,
                                   @RequestParam(value = "toDate", required = false) String toDate) throws Exception {
        userService.sendMessage(fromDate, toDate);
    }

    @GetMapping(value = "/user/personal_data", produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public PersonalDataResponseDto getPersonalData () {
        return userService.getPersonalData();
    }

    @PutMapping(value = "/user/data/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public void editUserData(@RequestBody EditUserDataRequestDto editUserDataRequestDto) throws CmdException {
        userService.editUserData(editUserDataRequestDto);
    }
}
