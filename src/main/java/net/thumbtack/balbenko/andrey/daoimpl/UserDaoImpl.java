package net.thumbtack.balbenko.andrey.daoimpl;

import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.domain.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

public class UserDaoImpl extends BaseDaoImpl implements UserDao {

    @Override
    public User addUser(User user) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        try (Session session = getSession()) {
            return session.createQuery("from User").list();
        }
    }

    @Override
    public User getUserByLogin(String login) {
        try (Session session = getSession()) {
            return (User) session.createQuery("from User where login='" + login + "'").uniqueResult();
        }
    }

    @Override
    public void deleteAllUsers() {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            session.createQuery("delete from User").executeUpdate();
            session.flush();
            session.getTransaction().commit();
        }
    }
}
