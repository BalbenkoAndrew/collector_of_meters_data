package net.thumbtack.balbenko.andrey.daoimpl;

import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.domain.Data;
import org.hibernate.Session;

import java.time.LocalDate;
import java.util.List;

public class DataDaoImpl extends BaseDaoImpl implements DataDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<Data> getAllData() {
        try (Session session = getSession()) {
            return session.createQuery("from Data").list();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Data> getUserData(LocalDate fromDate, LocalDate toDate, int id) {
        try (Session session = getSession()) {
            return session.createQuery("from Data where date >= '" + fromDate + "' and date <= '"+ toDate +"' and " + "user_id = " + id).list();
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public Data getUserDataByDate(LocalDate date, int userId) {
        try (Session session = getSession()) {
            return (Data) session.createQuery("from Data where date = '" + date + "' and user_id = '" + userId + "'").uniqueResult();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Data getUserDataByDateFromDate(LocalDate date, int userId) {
        try (Session session = getSession()) {
            return (Data) session.createQuery("from Data where date < '" + date + "' and user_id = '" + userId + "' order by date desc").setMaxResults(1).uniqueResult();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Data getUserDataByDateToDate(LocalDate date, int userId) {
        try (Session session = getSession()) {
            return (Data) session.createQuery("from Data where date > '" + date + "' and user_id = '" + userId + "' order by date").setMaxResults(1).uniqueResult();
        }
    }




    @Override
    public Data addUserData(Data data) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.save(data);
            session.getTransaction().commit();
        }
        return data;
    }

    @Override
    public void deleteAllData() {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            session.createQuery("delete from Data").executeUpdate();
            session.getTransaction().commit();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Data> getDataByMonth(LocalDate fromDate, LocalDate toDate) {
        try (Session session = getSession()) {
            return session.createQuery("from Data where date >= '" + fromDate + "' and date <= '" + toDate + "'").list();
        }
    }

    @Override
    public Data editUserData(Data data) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.update(data);
            session.getTransaction().commit();
        }
        return data;
    }
}
