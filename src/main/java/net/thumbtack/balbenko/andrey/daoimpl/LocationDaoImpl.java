package net.thumbtack.balbenko.andrey.daoimpl;

import net.thumbtack.balbenko.andrey.dao.LocationDao;
import net.thumbtack.balbenko.andrey.domain.Location;
import org.hibernate.Session;

import java.util.List;

public class LocationDaoImpl extends BaseDaoImpl implements LocationDao {

    @Override
    public Location addLocation(Location location) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.save(location);
            session.getTransaction().commit();
        }
        return location;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Location> getAllLocation() {
        try (Session session = getSession()) {
            return session.createQuery("from Location").list();
        }
    }

    @Override
    public void deleteAllLocations() {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            session.createQuery("delete from Location").executeUpdate();
            session.flush();
            session.getTransaction().commit();
        }
    }
}
