package net.thumbtack.balbenko.andrey.daoimpl;

import net.thumbtack.balbenko.andrey.utils.HibernateUtils;
import org.hibernate.Session;

public class BaseDaoImpl {
    protected Session getSession() {
        return HibernateUtils.getSessionFactory().openSession();
    }
}
