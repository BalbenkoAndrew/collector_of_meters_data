package net.thumbtack.balbenko.andrey.dao;

import net.thumbtack.balbenko.andrey.domain.Data;

import java.time.LocalDate;
import java.util.List;

public interface DataDao {

    public List<Data> getAllData();

    public List<Data> getDataByMonth(LocalDate fromDate, LocalDate toDate);

    public List<Data> getUserData(LocalDate fromDate, LocalDate toDate, int id);

    public Data addUserData(Data data);

    public void deleteAllData();

    public Data editUserData(Data data);

    public Data getUserDataByDate(LocalDate date, int userId);

    public Data getUserDataByDateFromDate(LocalDate date, int userId);

    public Data getUserDataByDateToDate(LocalDate date, int userId);
}
