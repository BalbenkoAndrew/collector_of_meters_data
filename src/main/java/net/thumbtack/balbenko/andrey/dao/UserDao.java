package net.thumbtack.balbenko.andrey.dao;

import net.thumbtack.balbenko.andrey.domain.User;

import java.util.List;

public interface UserDao {
    public User addUser(User user);

    public List<User> getAllUsers();

    public User getUserByLogin(String login);

    public void deleteAllUsers();
}
