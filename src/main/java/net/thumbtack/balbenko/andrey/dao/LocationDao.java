package net.thumbtack.balbenko.andrey.dao;

import net.thumbtack.balbenko.andrey.domain.Location;

import java.util.List;

public interface LocationDao {
    public Location addLocation(Location location);

    public List<Location> getAllLocation();

    public void deleteAllLocations();
}
