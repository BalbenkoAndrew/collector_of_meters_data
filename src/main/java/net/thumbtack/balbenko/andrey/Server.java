package net.thumbtack.balbenko.andrey;

import net.thumbtack.balbenko.andrey.config.security.SecurityConfig;
import net.thumbtack.balbenko.andrey.controllers.AdminController;
import net.thumbtack.balbenko.andrey.dao.DataDao;
import net.thumbtack.balbenko.andrey.dao.LocationDao;
import net.thumbtack.balbenko.andrey.dao.UserDao;
import net.thumbtack.balbenko.andrey.daoimpl.DataDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.LocationDaoImpl;
import net.thumbtack.balbenko.andrey.daoimpl.UserDaoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan({"net.thumbtack.balbenko.andrey"})
@Import({SecurityConfig.class})
@EnableSwagger2
@ComponentScan(basePackageClasses = {AdminController.class})
public class Server {
    @Bean
    public LocationDao locationDao() {
        return new LocationDaoImpl();
    }

    @Bean
    public UserDao userDao() {
        return new UserDaoImpl();
    }

    @Bean
    public DataDao dataDao() {
        return new DataDaoImpl();
    }

    public static void main(String[] args) {
        SpringApplication.run(Server.class, args);
    }

}
